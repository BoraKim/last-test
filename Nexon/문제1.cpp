#include "stdafx.h"

using ulonglong = unsigned long long;

ulonglong GetSmallN(ulonglong x)
{
	ulonglong smallPow = 0;
	for (int i = 1; i <= 63; ++i)
	{
		smallPow = (ulonglong)pow(2, i);
		if (smallPow < x)
			continue;

		return smallPow;
	}
	
	return 0;
}

ulonglong InputX()
{
	ulonglong x = 0;
	ulonglong maxPow = (ulonglong)pow(2, 63);

	while (true)
	{
		std::cin >> x;
		if (x <= 1 || maxPow <= x)
			continue;

		return x;
	}
}

int GetInputCount()
{
	const ulonglong maxCount = (ulonglong)pow(2, 18);
	int inputCount = 0;
	while (true)
	{
		std::cin >> inputCount;
		if (inputCount < 1 || maxCount < inputCount)
			continue;

		return inputCount;
	}
	return inputCount;
}

int MainQ1()
{
	int inputCount = GetInputCount();

	ulonglong x = 0;
	ulonglong allNPOT = 0;
	ulonglong smallNPOT = 0;
	for (int i = 0; i < inputCount; ++i)
	{
		x = InputX();
		smallNPOT = GetSmallN(x);
		allNPOT = allNPOT ^ smallNPOT;
	}

	std::cout << allNPOT << std::endl;
    return 0;
}