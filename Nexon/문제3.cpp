#include "stdafx.h"

#include <set>
#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <fstream>
#include <map>
#include <algorithm>
#include <list>
#include <map> 

enum T_DIR {
	NORTH = 0,
	EAST,
	SOUTH,
	WEST
};

int GetIntDirToString(std::string& D)
{
	static const std::string S_DIR[4] = { std::string("NORTH"),
		std::string("EAST"), std::string("SOUTH"), std::string("WEST") };

	for (int i = 0; i < 4; ++i)
	{
		if (S_DIR[i] == D)
			return i;
	}
	return -1;
}

namespace AStar
{
	struct Vec2i
	{
		int x, y;

		bool operator == (const Vec2i& coordinates_);
	};

	using uint = unsigned int;
	using CoordinateList = std::vector<Vec2i>;

	struct Node
	{
		uint G, H;
		Vec2i coordinates;
		Node *parent;

		Node(Vec2i coord_, Node *parent_ = nullptr);
		uint GetScore();
	};

	using NodeSet = std::set<Node*>;

	class Generator
	{
	public:
		Generator();
		void SetWorld(Vec2i worldSize_, std::vector < std::string >& MAP_);
		CoordinateList FindPath(Vec2i source_, Vec2i target_, int dir);
		std::string PrintPathDir(CoordinateList path, int startDir);

	private:
		bool DetectCollision(Vec2i coordinates_);
		bool IsVisitedNodeOnList(NodeSet& nodes_, Vec2i coordinates_);
		Node* GetVisitedNodeOnMap(std::multimap<int, Node*>& nodes_, Vec2i coordinates_);

	private:
		CoordinateList direction;
		Vec2i worldSize;
		std::vector < std::string > map;

	};

	static uint myHeuristic(Vec2i source_, Vec2i target_, int dir);
};

int GetDir(AStar::Vec2i pos, AStar::Vec2i n)
{
	if (pos.x == n.x) 
	{
		if (pos.y < n.y) 
			return EAST;

		return WEST;
	}

	if (pos.x > n.x)
		return NORTH;

	return SOUTH;
}

std::string PrintResult(int dir, int startDir) 
{

	if (dir == NORTH)
	{
		switch (startDir)
		{
		case EAST:   return std::string("LF"); break;
		case WEST:   return std::string("RF"); break;
		case SOUTH:   return std::string("!ERROR_1!"); break;
		case NORTH:   return std::string("F"); break;
		}
	}
	else if (dir == EAST)
	{
		switch (startDir)
		{
		case EAST:   return std::string("F"); break;
		case WEST:   return std::string("!ERROR_2!"); break;
		case SOUTH:   return std::string("LF"); break;
		case NORTH: return std::string("RF"); break;
		}
	}
	else if (dir == SOUTH)
	{
		switch (startDir)
		{
		case EAST:   return std::string("RF"); break;
		case WEST:   return std::string("LF"); break;
		case SOUTH:   return std::string("F"); break;
		case NORTH:   return std::string("!ERROR_3!"); break;
		}
	}
	else if (dir == WEST)
	{
		switch (startDir) 
		{
		case EAST:   return std::string("!ERROR_4!"); break;
		case WEST:   return std::string("F"); break;
		case SOUTH:   return std::string("RF"); break;
		case NORTH:   return std::string("LF"); break;
		}
	}
	return "!ERROR_5!";
}

bool AStar::Vec2i::operator == (const Vec2i& coordinates_)
{
	return (x == coordinates_.x && y == coordinates_.y);
}

AStar::Vec2i operator + (const AStar::Vec2i& left_, const AStar::Vec2i& right_)
{
	return{ left_.x + right_.x, left_.y + right_.y };
}

AStar::Node::Node(Vec2i coordinates_, Node *parent_)
{
	parent = parent_;
	coordinates = coordinates_;
	G = H = 0;
}

AStar::uint AStar::Node::GetScore()
{
	return G + H;
}

AStar::Generator::Generator()
{
	direction = {
		{ 0, 1 },{ 1, 0 },{ 0, -1 },{ -1, 0 }
	};
}

void AStar::Generator::SetWorld(Vec2i worldSize_, std::vector < std::string >& MAP_)
{
	worldSize = worldSize_;
	map.assign(MAP_.begin(), MAP_.end());
}

AStar::CoordinateList AStar::Generator::FindPath(Vec2i source_, Vec2i target_, int dir)
{
	Node *current = nullptr;
	NodeSet closedSet;
	std::multimap<int, Node*> openMap;
	Node* start = new Node(source_);
	openMap.insert(std::make_pair(start->GetScore(), start));

	while (!openMap.empty()) 
	{
		auto itr = openMap.begin();
		current = itr->second;
		if (current->coordinates == target_) {
			break;
		}

		closedSet.insert(current);
		openMap.erase(itr);
		for (uint i = 0; i < 4; ++i) {

			Vec2i newCoordinates(current->coordinates + direction[i]);

			if (DetectCollision(newCoordinates) ||
				IsVisitedNodeOnList(closedSet, newCoordinates)) {
				continue;
			}

			int nextDir = GetDir(current->coordinates, newCoordinates);

			uint totalCost = current->G + ((nextDir == dir) ? 10 : 14);

			Node *successor = GetVisitedNodeOnMap(openMap, newCoordinates);
			if (successor == nullptr) 
			{
				successor = new Node(newCoordinates, current);

				if (current->parent)
				{
					dir = GetDir(current->parent->coordinates, current->coordinates);
				}

				successor->H = myHeuristic(current->coordinates, newCoordinates, dir);

				totalCost += successor->H;
				successor->G = totalCost;
				openMap.insert(std::make_pair(successor->GetScore(), successor));
			}
			else if (totalCost < successor->G) 
			{
				successor->parent = current;
				successor->H = myHeuristic(current->coordinates, newCoordinates, dir);
				totalCost += successor->H;
				successor->G = totalCost;
			}
		}
	}

	CoordinateList path;
	while (current != nullptr) {
		path.push_back(current->coordinates);
		current = current->parent;
	}

	return path;
}

bool AStar::Generator::IsVisitedNodeOnList(NodeSet& nodes_, Vec2i coordinates_)
{
	auto itr = std::find_if(nodes_.begin(), nodes_.end(), 
		[&coordinates_](Node* x) 
	{
		return x->coordinates == coordinates_;
	});
	
	return itr != nodes_.end() ? true : false;
}

AStar::Node* AStar::Generator::GetVisitedNodeOnMap(std::multimap<int, Node*>& nodes_, Vec2i coordinates_)
{
	auto itr = std::find_if(nodes_.begin(), nodes_.end(),
		[&coordinates_](const std::pair<int, Node*> & t) -> bool {
		return t.second->coordinates == coordinates_;
	}
	);

	return itr != nodes_.end() ? itr->second : nullptr;
}

bool AStar::Generator::DetectCollision(Vec2i coordinates_)
{
	if (coordinates_.x < 0 || coordinates_.x >= worldSize.x || coordinates_.y < 0 || coordinates_.y >= worldSize.y) {
		return true;
	}

	std::string line = map[coordinates_.x];
	static const std::string block("#");
	std::string temp;
	temp.insert(0, 1, line.at(coordinates_.y));
	if (temp == block)
		return true;

	return false;
}

AStar::uint AStar::myHeuristic(Vec2i source_, Vec2i target_, int dir)
{
	int nextDir = GetDir(source_, target_);
	if (dir == nextDir) return 0;

	if ((dir + 2) % 4 == nextDir)   return 99999999;

	return 10;
}

std::string AStar::Generator::PrintPathDir(CoordinateList path, int startDir)
{
	std::reverse(path.begin(), path.end());
	auto itr = path.begin();
	Vec2i pos = *itr;
	std::string way;
	for (auto i = itr + 1; i != path.end(); i++) {

		int dir = GetDir(pos, (*i));
		way += PrintResult(dir, startDir);

		startDir = dir;
		pos = (*i);
	}
	return way;
}

std::string Solve(std::string D, int W, std::vector < std::string > MAP)
{
	AStar::Generator generator;
	AStar::Vec2i worldSize;
	worldSize.x = (int)MAP.size();
	worldSize.y = W;
	generator.SetWorld(worldSize, MAP);

	AStar::Vec2i startPos, endPos;

	for (std::size_t i = 0; i < MAP.size(); ++i)
	{
		std::string line = MAP[i];
		auto pos = line.find("T");
		if (pos != std::string::npos)
		{
			startPos.x = i;
			startPos.y = pos;
		}
		pos = line.find("G");
		if (pos != std::string::npos)
		{
			endPos.x = i;
			endPos.y = pos;
		}
	}

	auto path = generator.FindPath(startPos, endPos, GetIntDirToString(D));
	return generator.PrintPathDir(path, GetIntDirToString(D));
}

int MainQ3()
{
	std::string res;
	std::string _D("SOUTH");

	int _W = 8;
	int _MAP_size = 8;

	std::vector<std::string> _MAP;
	_MAP.push_back(".......G");
	_MAP.push_back("........");
	_MAP.push_back("...#####");
	_MAP.push_back("........");
	_MAP.push_back("..#####.");
	_MAP.push_back("##...##.");
	_MAP.push_back("...#....");
	_MAP.push_back("T..#....");

	res = Solve(_D, _W, _MAP);
	std::cout << res << std::endl;

	return 0;
}