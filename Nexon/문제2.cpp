#include "stdafx.h"

using ulonglong = unsigned long long;

int MyPow(ulonglong increase, ulonglong time, int mod)
{
	int ret = 1;
	while (time != 0) {
		if (time & 1)
			ret = (ret * increase) % mod;
		time >>= 1;
		increase = (increase * increase) % mod;
	}

	return ret;
}

int MainQ2() 
{
	/* Enter your code here. Read input from STDIN. Print output to STDOUT */
	ulonglong infectee = 0, increase = 0, time = 0;
	ulonglong maxT = (ulonglong)pow(10, 19);

	int mod = pow(10, 9) + 7;

	while (true)
	{
		std::cin >> infectee >> increase >> time;
		if (infectee < 1 || 100 < infectee)
			continue;

		if (increase < 1 || 100 < increase)
			continue;

		if (time < 1 || maxT < time)
			continue;

		int result = infectee * MyPow(increase, time, mod) % mod;
		std::cout << result << std::endl;
		break;
	}
	return 0;
}
