#include "stdafx.h"

struct Enemy {
	int id;
	int x;
	int y;
	int radius;
	bool isDead;
	Enemy(int id_, int x_, int y_, int redius_) : 
		id(id_), x(x_), y(y_), radius(redius_), isDead(false)
	{

	}
};

void DoAttack(const Enemy* attaker, const Enemy* target, std::list<Enemy*>& enemyList, std::vector<int>& deadList)
{
	// 직선의 방적식 ax + by - c =0;
	int a = attaker->y - target->y;
	int b = target->x - attaker->x;
	int c = attaker->x * target->y - target->x * attaker->y;

	int x = attaker->x - target->x;
	int y = attaker->y - target->y;

	int deadCount = 0;
	for (auto itr = enemyList.begin(); itr != enemyList.end();)
	{
		if (*itr == attaker)
		{
			++itr;
			continue;
		}

		Enemy* other = *itr;

		// 적의 중점으로부터 직선의 거리
		// 점과 직선 사이의 거리 방정식
		float dist = (float)abs(a * other->x + b * other->y + c) / (float)sqrt(a * a + b * b);
		if (dist <= other->radius && other->isDead == false)
		{
			int tx = attaker->x - other->x;
			int ty = attaker->y - other->y;
			
			// 화살 방향을 계산해야 한다.
			int dot = x * tx + y * ty;
			if (dot < 0)
			{
				++itr;
				continue;
			}

			other->isDead = true;
			deadList.push_back(other->id);
			itr = enemyList.erase(itr);

			if (4 <= ++deadCount)
				return;

			continue;
		}
		++itr;
	}
}

int MainQ4()
{
	std::map<int, Enemy*> enemyMap;
	std::list<Enemy*> enemyList;
	std::vector<int> deadList;
	while (true)
	{
		int inputCount = 0;
		std::cin >> inputCount;

		for (int i = 0; i < inputCount; ++i)
		{
			int id, x, y, radius;
			std::cin >> id>> x >> y >> radius;

			auto itr = enemyMap.insert(std::pair<int, Enemy*>(id, new Enemy(id, x, y, radius)));
		}

		for (auto iter = enemyMap.begin(); iter != enemyMap.end(); ++iter)
			enemyList.emplace_back(iter->second);

		int shotCount = 0;
		std::cin >> shotCount;
		for (int i = 0; i < shotCount; ++i)
		{
			int attakerID = 0 , targetID = 0;
			std::cin >> attakerID >> targetID;

			Enemy* attaker = enemyMap[attakerID];
			if (attaker == nullptr || attaker->isDead)
				continue;

			enemyList.sort([&attaker](Enemy* a, Enemy* b) 
			{
				int dist_a = ((a->x - attaker->x) * (a->x - attaker->x)) + ((a->y - attaker->y) * (a->y - attaker->y));
				int dist_b = ((b->x - attaker->x) * (b->x - attaker->x)) + ((b->y - attaker->y) * (b->y - attaker->y));
				return dist_a < dist_b; 
			});


			DoAttack(attaker, enemyMap[targetID], enemyList, deadList);
		}
		break;
	}

	for (std::size_t i = 0; i < deadList.size(); ++i)
	{
		std::cout << deadList[i] << std::endl;;
	}
	return 0;
}